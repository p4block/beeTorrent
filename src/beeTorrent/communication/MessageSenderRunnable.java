package beeTorrent.communication;

import beeTorrent.packets.MessagePacket;
import beeTorrent.roles.Bee;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

import static beeTorrent.utils.ioUtils.ep;

public class MessageSenderRunnable implements Runnable {

    private Bee mCaller;
    private Socket mSocket = null;
    private MessagePacket mMessagePacket;

    /**
     * Message Sender Runnable Method.
     * @param caller Bee caller
     * @param IP IP of caller
     * @param port Port of caller
     * @param message MessagePacket
     */
    public MessageSenderRunnable(Bee caller, InetAddress IP, int port, MessagePacket message) {
        try {
            this.mCaller = caller;
            this.mSocket = new Socket(IP, port);
            this.mMessagePacket = message;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Threable method for sending and receiving messages
     */
    public void run() {
        try {
            // Sending
            ObjectOutputStream mObjectOutputStream = new ObjectOutputStream(mSocket.getOutputStream());
            mObjectOutputStream.writeObject(mMessagePacket);

            // Receiving
            ObjectInputStream mObjectInputStream = new ObjectInputStream(mSocket.getInputStream());
            MessagePacket mReceivedPacket = (MessagePacket) mObjectInputStream.readObject();

            // Handling
            mCaller.handleResponse(mSocket, mReceivedPacket);

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
