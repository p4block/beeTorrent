package beeTorrent.utils;

import beeTorrent.roles.Bee;
import beeTorrent.roles.Peer;
import beeTorrent.roles.Tracker;
import beeTorrent.shell.Shell;

import java.util.ArrayList;
import java.util.List;

public class lifeCycleUtils {

    /**
     * Keep track of how many we got. Peer List
     */
    public static List<Peer> mPeerList = new ArrayList<>();
    /**
     * Keep track of how many we got. Tracker List
     */
    public static List<Tracker> mTrackerList = new ArrayList<>();

    /**
     * Clean up before exiting program
     * @param i
     */
    public static void exitMainLoop(int i){
        // TODO: Do cleanup
        System.exit(i);
    }

    /**
     * Starting role Peer on default port
     */
    public static void startPeer() {
        Peer mPeer = new Peer();
        mPeerList.add(mPeer);
    }

    /**
     * Starting role Peer on selected port
     * @param port selected port
     */
    public static void startPeer(int port) {
        Peer mPeer = new Peer(port);
        mPeerList.add(mPeer);
    }

    /**
     * Starting role Tracker on selected port
     * @param port selected port
     */
    public static void startTracker(int port) {
        Tracker mTracker = new Tracker(port);
        mTrackerList.add(mTracker);
    }

    /**
     * Get Bee working in selected port
     * @param port port to know Bee
     * @return Bee
     */
    public static Bee getBee(int port){
        for(Bee mBee : mPeerList){
            if(mBee.mPort == port){
                return mBee;
            }
        }

        for(Bee mBee : mTrackerList){
            if(mBee.mPort == port){
                return mBee;
            }
        }

        return null;
    }

    /**
     * Stopping roles
     * @param bee selected to stop
     */
    public static void stopBee(Bee bee){
        bee.stop();
        mPeerList.remove(bee);
        mTrackerList.remove(bee);
        bee = null;
    }

    /**
     * Method to start the shell
     */
    public static void startShell() {
        Shell.mainLoop();
    }

}
