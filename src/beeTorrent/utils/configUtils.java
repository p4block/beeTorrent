package beeTorrent.utils;

import java.net.InetAddress;

import static beeTorrent.utils.ioUtils.ep;

public class configUtils {

    /**
     * Systemwide debug var
     */
    public static boolean debugMode = false;

    /**
     * Default Port
     */
    public static int DEFAULT_PORT = 6969;

    /**
     * Minimum Port assignable
     */
    public static int MIN_PORT = 1024;

    /**
     * Maximum Port assignable
     */
    public static int MAX_PORT = 49151;

    /**
     * Maximum Port assignable
     */
    public static int CHUNK_SIZE = 512;

    /**
     * Peer selection param
     */
    public enum PeerSelectionMode {
        SINGLE,
        RANDOM,
        RAREST
    }

    public static PeerSelectionMode mPeerSelectionMode = PeerSelectionMode.RANDOM;

    @Deprecated
    /**
     * Instance for IP
     */
    public static InetAddress mIPAddr;

    @Deprecated
    /**
     * Instance for hostname
     */
    public static String  mHostname;

    /**
     * Work folder
     */
    //public static String workFolder = System.getProperty("user.home");
    public static String workFolder;

    /**
     * Validate selected port
     * @param port port for validate
     * @return port validated
     */
    public static int validatePort(int port) {
        if (port > MIN_PORT & port < MAX_PORT) {
            return port;
        } else {
            ep("Invalid port. Only available ports on range 1024 <-> 49151");
            ep("Setting default values");
            return DEFAULT_PORT;
        }
    }
}
