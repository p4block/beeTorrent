package beeTorrent.packets;

import java.io.Serializable;

public class ChunkPacket extends MessagePacket implements Serializable  {

    /**
     * Flag to know if is a request
     */
    public boolean IsRequest;
    public String FileName;

    /**
     * File Hash
     */
    public String FileHash;

    /**
     * Chunk hash
     */
    public String ChunkHash;

    /**
     * File Chunk
     */
    public byte[] FileChunk;
    public int Offset;

    /**
     * Constructor Method. ChunkPacket generate
     * @param fileHash Hash of file
     * @param chunkHash Chunk Hash
     */
    public ChunkPacket(String fileName, String fileHash, String chunkHash, int offset) {
        super.Type = MessageType.CHUNK;
        IsRequest = true;
        FileName = fileName;
        FileHash = fileHash;
        ChunkHash = chunkHash;
        Offset = offset;
    }

    /**
     * Constructor Method. ChunkPacket generate
     * @param fileHash Hash of file
     * @param chunkHash Chunk Hash
     * @param fileChunk Chunk of file
     */
    public ChunkPacket(String fileName, String fileHash, String chunkHash, byte[] fileChunk, int offset) {
        super.Type = MessageType.CHUNK;
        IsRequest = false;
        FileName = fileName;
        FileHash = fileHash;
        ChunkHash = chunkHash;
        FileChunk = fileChunk;
        Offset = offset;
    }


    /**
     * Print type
     */
    @Override
    public void Print() {
        System.out.println("Type:   " + Type);
        System.out.println("FileName:   " + FileName);
        System.out.println("FileHash:   " + FileHash);
        System.out.println("ChunkHash:   " + ChunkHash);
    }

}