package beeTorrent.roles;

import beeTorrent.files.SharedFile;
import beeTorrent.files.SwarmItem;
import beeTorrent.packets.*;
import beeTorrent.utils.configUtils;
import beeTorrent.utils.docUtils;
import beeTorrent.utils.ioUtils;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static beeTorrent.utils.ioUtils.el;
import static beeTorrent.utils.ioUtils.ep;

public class Peer extends Bee {

    /**
     * List of shared files
     */
    private List<SharedFile> mSharedFilesList = new ArrayList<>();
    /**
     * List of swarm items
     */
    private List<SwarmItem> mSwarmItemList = new ArrayList<>();

    private List<SharedFile> availableFileList = new ArrayList<>();

    /**
     * Registered Tracker (SwarmItem)
     */
    public SwarmItem mRegisteredTracker;

    /**
     * Constructor Peer without args
     */
    public Peer(){
        // Starting without a specified port will instead bind to a random user port, like a proper user application.
        super(HiveRole.PEER, 0);
    }

    /**
     * Constructor Peer
     * @param port Port to link
     */
    public Peer(int port){
        super(HiveRole.PEER, port);
    }

    /**
     * Method to setup working directory. Show shared files
     */
    void prepareWorkDir() {
        ioUtils.el(mThis, "Generating shared file list", true);
        el(mThis, "Work folder: " + ioUtils.generateWorkFolder(mThis), true);
        try {
            for(File file : ioUtils.generateFileList(ioUtils.generateWorkFolder(mThis)) ) {
                if (file.isFile()) {
                    mSharedFilesList.add(new SharedFile(file));
                    ioUtils.el(mThis, "Added " + file.getPath(), true);
                }
            }
        } catch (Exception e) {
            ioUtils.el(mThis, "Generating shared file list", true);
            ep(ioUtils.generateWorkFolder(mThis).getPath());
            // Empty work folder
        }
    }

    // Functions
    public void registerOnTracker(InetAddress IP, int port) {
        sendMessage(IP, port, new RegisterPacket(mUUID, mRole, mPort));
    }

    /**
     * Register on tracker on default port
     * @param IP Tracker IP
     */
    public void registerOnTracker(InetAddress IP) {
        sendMessage(IP, 6969, new RegisterPacket(mUUID, mRole, mPort));
    }

    /**
     * Ping Bee for check connection
     * @param IP Destination IP
     * @param port Destination Port
     * @param message Message to send
     */
    public void pingBee(InetAddress IP, int port, String message) {
        sendMessage(IP, port, new PingPacket(mUUID, message));
    }

    /**
     * Send Files list to tracker
     */
    public void sendFilesList() {
        sendMessage(mRegisteredTracker.IP, mRegisteredTracker.Port, new FileListPacket(mUUID, mSharedFilesList));
    }

    /**
     * Get Swarm List. Ask tracker for it.
     */
    public void getSwarmList() {
        sendMessage(mRegisteredTracker.IP, mRegisteredTracker.Port, new GetPacket(mUUID, GetPacket.getTarget.PEERS));
    }

    /**
     * Print useful info.
     */
    public void printInfo() {
        ep("Registered tracker: "+ mRegisteredTracker.toString());
        ep("Shared files: "+ mSharedFilesList);
        ep("Available files on swarm: " + getAvailableFiles());
        ep("Other known peers: " + mSwarmItemList);
    }

    // Also populates the list for later
    private List<SharedFile> getAvailableFiles() {
        for (SwarmItem si : mSwarmItemList) {
            try {
                for (SharedFile sharedFile : si.mSharedFilesList) {
                    if (!availableFileList.contains(sharedFile)) {
                        availableFileList.add(sharedFile);
                    }
                }
            } catch (Exception e) {
                // Peer is sharing nothing yet
                //e.printStackTrace();
            }
        }

        return availableFileList;
    }

    public void printAvailableFiles() {
        int i = 0;
        for (SharedFile sf : availableFileList) {
            ep("[" + i + "] " + sf.toString());
            i++;
        }
    }

    public void downloadFile(int number) {
        // Get hash of file the user wants
        SharedFile wantedFile = availableFileList.get(number);

        // Find peers who have the file
        List<SwarmItem> listOfPeersWhoActuallyHaveTheFile = new ArrayList<>();


        for (SwarmItem si : mSwarmItemList) {
            try {
                if (si.mSharedFilesList.contains(wantedFile)) {
                    listOfPeersWhoActuallyHaveTheFile.add(si);
                }
            } catch (Exception e) {
                // Peer isn't sharing anything
            }
        }

        // Query their available blocks
        // TODO: we are assuming the peer has the whole file for the moment
//        for (SwarmItem swarmItem : listOfPeersWhoActuallyHaveTheFile) {
//            sendMessage()


        // Get file blocks
        // TODO: Parse picking mode from commandline
        configUtils.PeerSelectionMode mPeerSelectionMode = configUtils.mPeerSelectionMode;

        SwarmItem targetPeer;

        switch (mPeerSelectionMode) {
            case SINGLE:
                targetPeer = listOfPeersWhoActuallyHaveTheFile.get(0);

                for (int i = 0; i < wantedFile.chunkHashList.length ; i++) {
                    ChunkPacket chunkPacket =
                            new ChunkPacket(wantedFile.name, wantedFile.fileHash, wantedFile.chunkHashList[i], i);

                    sendMessage(targetPeer.IP, targetPeer.Port, chunkPacket);
                }
                break;
            case RANDOM:
                for (int i = 0; i < wantedFile.chunkHashList.length ; i++) {
                    Random rand = new Random();
                    targetPeer = listOfPeersWhoActuallyHaveTheFile.get(
                            rand.nextInt(listOfPeersWhoActuallyHaveTheFile.size()));

                    ChunkPacket chunkPacket =
                            new ChunkPacket(wantedFile.name, wantedFile.fileHash, wantedFile.chunkHashList[i], i);

                    sendMessage(targetPeer.IP, targetPeer.Port, chunkPacket);
                }
                break;

        }

    }

    /**
     * Message Handler. Handle incoming connections
     * @param socket Port for receiving incoming message
     * @param messagePacket Message packet
     * @return messagePacket Message packet
     */
    @Override
    public MessagePacket handleMessage(Socket socket, MessagePacket messagePacket) {

        // Decode message
        MessagePacket.MessageType mType = messagePacket.Type;

        switch (mType){
            case CHUNK:
                ChunkPacket mChunkPacket = (ChunkPacket) messagePacket;
                if(mChunkPacket.IsRequest) {
                    el(mThis, "Received chunk request");
                    return new ChunkPacket(mChunkPacket.FileName, mChunkPacket.FileHash, mChunkPacket.ChunkHash,
                            getFileChunk(mChunkPacket.FileHash, mChunkPacket.ChunkHash),
                            mChunkPacket.Offset);
                } else {
                    return new MessagePacket(super.mUUID, MessagePacket.MessageType.DANKNT);
                }
            case PING:
                PingPacket mPingPacket = (PingPacket) messagePacket;
                return new PingPacket(mUUID, mPingPacket.Content);
            default:
                return new MessagePacket(super.mUUID, MessagePacket.MessageType.DANKNT);
        }
    }

    /**
     * Method run when message sent has been responded
     * @param socket Port for receiving incoming message
     * @param messagePacket Message
     */
    @Override
    public void handleResponse(Socket socket, MessagePacket messagePacket) {

        // Decode message
        MessagePacket.MessageType mType = messagePacket.Type;

        switch (mType){
            case REGISTER:
                RegisterPacket mRegisterPacket = (RegisterPacket) messagePacket;
                if (mRegisterPacket.Role == HiveRole.TRACKER) {
                    mRegisteredTracker = new SwarmItem(mRegisterPacket.mUUID,
                            mRegisterPacket.Role, socket.getInetAddress(), mRegisterPacket.Port);
                    el(mThis, "Registed on tracker " + socket.getInetAddress() + ":" + mRegisterPacket.Port);
                }
                break;
            case PEERLIST:
                PeerListPacket mPeerListPacket = (PeerListPacket) messagePacket;
                mSwarmItemList = mPeerListPacket.mPeerList;
                el(mThis, "Received peer list: "+ mSwarmItemList, true);
                break;
            case CHUNK:
                ChunkPacket mChunkPacket = (ChunkPacket) messagePacket;
                if (!mChunkPacket.IsRequest) {
                    el(mThis, "Received file chunk" + mChunkPacket.Offset, true);
                    handleFileChunk(mChunkPacket.FileName, mChunkPacket.FileHash,
                            mChunkPacket.ChunkHash, mChunkPacket.FileChunk, mChunkPacket.Offset);
                }
                break;
            case PING:
                el(mThis, ((PingPacket)messagePacket).Content,true);
                break;
        }
    }

    private byte[] getFileChunk (String fileHash, String chunkHash) {
        byte[] chunk;

        for (SharedFile sf : mSharedFilesList) {
            if (sf.fileHash.equals(fileHash)) {
                for (int offset = 0; offset < sf.chunkHashList.length; offset++) {
                    if (sf.chunkHashList[offset].equals(chunkHash)) {
                        // Target block reached
                        // Get the byte[]
                        try {
                            File targetFile = new File(sf.path);

                            chunk = ioUtils.getByteArrayFromFile(targetFile, offset);

                            return chunk;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        return null;

    }

    private void handleFileChunk(String fileName, String fileHash, String chunkHash, byte[] fileChunk, int offset) {
        try {
            // Create download folder
            File outputFolder = new File(ioUtils.generateWorkFolder(mThis) + "/incoming");
            outputFolder.mkdirs();

            el(mThis, "Created download folder at " + ioUtils.generateWorkFolder(mThis) + "/incoming", true);

            // TODO: check hashes, although TCP does it for us, so it's of little use

            File file = new File(ioUtils.generateWorkFolder(mThis) + "/incoming/" + fileName);
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
            randomAccessFile.seek(configUtils.CHUNK_SIZE * 1024 * offset);
            randomAccessFile.write(fileChunk);
            randomAccessFile.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
