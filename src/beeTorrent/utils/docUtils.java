package beeTorrent.utils;

import static beeTorrent.utils.ioUtils.*;

public class docUtils {

    /**
     * String CLI Help
     */
    public static String mCmdLineSyntax = "java -jar " + getJarName() + " -r <role> -p <port>";

    /**
     * Help helper
     * @param input reference to print help message
     */
    public static void printHelp(String input){
        switch(input){
            case "masterShell":
                ep("Available commands:");
                ep("list    <peers|trackers>");
                ep("start   <peer|tracker> <port>");
                ep("stop    <port>");
                ep("log     <port>");
                ep("shell   <port>");
                ep("exit");
                ep("help");
                break;
            case "peerShell":
                ep("Available commands:");
                ep("ping <ip> <port>");
                ep("register <ip> <port>");
                ep("list    : Shows available information");
                ep("share   : Sends file list to tracker");
                ep("peers   : Asks the tracker for peers");
                ep("log     : Reads the log");
                ep("exit");
                ep("help");
                break;
            case "trackerShell":
                ep("Available commands:");
                ep("list");
                ep("exit");
                ep("help");
                break;
            case "start":
                ep("Missing what to start or invalid data");
                break;
            case "stop":
                ep("Invalid port or nothing running on it");
                break;
            case "welcome":
                ep("\n=== beeTorrent CLI launcher ===\n");
                break;
        }
    }
}
