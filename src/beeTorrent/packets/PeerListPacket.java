package beeTorrent.packets;

import beeTorrent.files.SwarmItem;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class PeerListPacket extends MessagePacket implements Serializable  {

    /**
     * List of Peer on Swarm
     */
    public List<SwarmItem> mPeerList;

    /**
     * Constructor Method.
     * @param uuid UUID set
     * @param peerList List of peer
     */
    public PeerListPacket(UUID uuid, List<SwarmItem> peerList) {
        super.Type = MessageType.PEERLIST;
        super.mUUID = uuid;
        mPeerList = peerList;
    }

    /**
     * Print type
     */
    public void Print() {
        System.out.println("Type:   " + Type);
    }

}