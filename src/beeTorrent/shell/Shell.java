package beeTorrent.shell;

import beeTorrent.roles.Bee;
import beeTorrent.roles.Peer;
import beeTorrent.roles.Tracker;
import beeTorrent.utils.docUtils;
import beeTorrent.utils.configUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

import static beeTorrent.utils.configUtils.debugMode;
import static beeTorrent.utils.docUtils.*;
import static beeTorrent.utils.ioUtils.*;
import static beeTorrent.utils.lifeCycleUtils.*;

public class Shell {

    /**
     * Shell target types
     */
    public enum shellTarget {
        tracker,
        peer,
        beeTorrent,
    }

    /**
     * Refer to this target
     */
    private static shellTarget mShellTarget = shellTarget.beeTorrent;

    /**
     * Target Bee for shell
     */
    private static Bee mBee;

    /**
     * Main loop shell
     */
    public static void mainLoop() {

        // Text prompt
        String mPrompt;

        while (true) {

            // Sleep to avoid race condition bugs (lol)
            try {
                Thread.sleep(31);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // Generate prompt
            if (mShellTarget != shellTarget.beeTorrent) {
                //mPrompt = "↳ " + mBee.mRole + mBee.mPort + "@" + mBee.mIP;
                mPrompt = "↳ " + mBee.mRole + mBee.mPort;
            } else {
                mPrompt = "↳ " + mShellTarget;
            }

            System.out.println(mPrompt);

            // Store user input, dangerously.
            String[] params;

            try {
                params = mKbScanner.nextLine().split(" ");

                // Force array to be at least of size 4 to avoid complexity later (entropy++)
                if (params.length < 2) {
                    params = new String[]{params[0], " "};
                }

                if (params.length < 3) {
                    params = new String[]{params[0],params[1], " "};
                }

                if (params.length < 4) {
                    params = new String[]{params[0],params[1], params[2], " "};
                }

                // Run appropriate interface
                switch (mShellTarget) {
                    case beeTorrent:
                        mainShell(params);
                        break;
                    case tracker:
                        trackerShell(params, (Tracker) mBee);
                        break;
                    case peer:
                        peerShell(params, (Peer) mBee);
                        break;
                }

            } catch (Exception e) {
                if (debugMode) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Main Loop CLI Shell
     * @param params execute params
     */
    private static void mainShell(String[] params) {

        switch (params[0]) {
            case "start":
                switch (params[1]) {
                    case "peer":
                        try {
                            if (!params[2].equals(" ")) {
                                int port = Integer.parseInt(params[2]);
                                startPeer(configUtils.validatePort(port));

                            } else {
                                startPeer();
                            }
                        } catch (NumberFormatException e) {
                            ep("Invalid port number");
                        }
                        break;
                    case "tracker":
                        try {
                            int port = Integer.parseInt(params[2]);
                            startTracker(configUtils.validatePort(port));

                        } catch (NumberFormatException e){
                            if (!params[2].equals(" ")) {
                                ep("Invalid port number");
                            }
                            startTracker(configUtils.DEFAULT_PORT);
                        }
                        break;
                    default:
                        docUtils.printHelp("start");
                        break;
                }
                break;
            case "list" :
                switch (params[1]) {
                    case "peers" :
                        ep("Peers: " + Arrays.toString(mPeerList.toArray()));
                        break;
                    case "trackers" :
                        ep("Trackers" + Arrays.toString(mTrackerList.toArray()));
                        break;
                    default:
                        ep("Peers:    " + Arrays.toString(mPeerList.toArray()));
                        ep("Trackers: " + Arrays.toString(mTrackerList.toArray()));
                        break;
                }
                break;
            case "stop":
                try {
                    mBee = getBee(Integer.parseInt(params[1]));
                } catch (NumberFormatException e) {
                    mBee = null;
                }
                if (mBee != null) {
                    stopBee(mBee);
                } else {
                    docUtils.printHelp("stop");
                }
                break;
            case "shell":
                try {
                    mBee = getBee(Integer.parseInt(params[1]));
                } catch (NumberFormatException e) {
                    mBee = null;
                }
                if (mBee != null) {
                    if (mBee instanceof Tracker) {
                        mShellTarget = shellTarget.tracker;
                    } else if (mBee instanceof Peer) {
                        mShellTarget = shellTarget.peer;
                    } else {
                        ep("Oopsie whoopsie, you just discovered a little buggie!");
                        docUtils.printHelp("stop");
                    }
                } else {
                    docUtils.printHelp("stop");
                }
                break;
            case "log":
                try {
                    mBee = getBee(Integer.parseInt(params[1]));
                } catch (NumberFormatException e) {
                    mBee = null;
                }
                if (mBee != null) {
                    ep(logRead(mBee));
                } else {
                    docUtils.printHelp("log");
                }
                break;
            case "exit":
                exitMainLoop(0);
                break;
            case "":
                break;
            default:
                printHelp("masterShell");
                break;
        }
    }

    /**
     * Peer Shell CLI
     * @param params execute params
     * @param peer Peer CLI
     */
    private static void peerShell(String[] params, Peer peer) {

        switch (params[0]) {
            case "register":
                try {
                    if (!params[2].equals(" ")) {
                        peer.registerOnTracker(InetAddress.getByName(params[1]), Integer.parseInt(params[2]));
                    } else {
                        peer.registerOnTracker(InetAddress.getByName(params[1]));
                    }
                } catch (UnknownHostException e) {
                    printHelp("peerShell");
                    if (debugMode) {
                        e.printStackTrace();
                    }
                }
                break;
            case "ping":
                try {
                    peer.pingBee(InetAddress.getByName(params[1]), Integer.parseInt(params[2]), params[3]);
                } catch (UnknownHostException e) {
                    printHelp("peerShell");
                    if (debugMode) {
                        e.printStackTrace();
                    }
                }
                break;
            case "list":
                if (peer.mRegisteredTracker != null) {
                    peer.printInfo();
                } else {
                    ep("Register to a tracker first");
                }
                break;
            case "share":
                if (peer.mRegisteredTracker != null) {
                    peer.sendFilesList();
                } else {
                    ep("Register to a tracker first");
                }
                break;
            case "peers":
                if (peer.mRegisteredTracker != null) {
                    peer.getSwarmList();
                } else {
                    ep("Register to a tracker first");
                }
                break;
            case "download":
                if (peer.mRegisteredTracker != null) {
                    peer.printAvailableFiles();
                    ep("Type name of file to download:");
                    int fileIndex = Integer.valueOf(mKbScanner.nextLine());
                    peer.downloadFile(fileIndex);
                    //peer.getAvailableFiles();
                } else {
                    ep("Register to a tracker first");
                }
                break;
            case "log":
                ep(logRead(peer));
                break;
            case "exit":
                mShellTarget = shellTarget.beeTorrent;
                break;
            case "":
                break;
            default:
                printHelp("peerShell");
        }

    }

    /**
     * Tracker CLI Shell.
     * @param params execute params
     * @param tracker tracker CLI
     */
    private static void trackerShell(String[] params, Tracker tracker) {

        switch (params[0]) {
            case "list":
                ep(tracker.mPeerList.toString());
                break;
            case "log":
                ep(logRead(tracker));
                break;
            case "exit":
                mShellTarget = shellTarget.beeTorrent;
                break;
            case "":
                break;
            default:
                printHelp("trackerShell");
        }

        // Make sure that command output is not sent to current line
        //System.out.print("\n");
    }
}
