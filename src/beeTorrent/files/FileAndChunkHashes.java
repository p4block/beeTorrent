package beeTorrent.files;

import java.io.Serializable;

public class FileAndChunkHashes implements Serializable {

    /**
     * String with File Hash
     */
    String mFileHash;

    /**
     * Array for all chunk Hashes
     */
    String[] mChunkHashesArray;

    /**
     * Constructor For assign the File Hash and all chunk hash array
     * @param fileHash
     * @param chunkHashesArray
     */
    public FileAndChunkHashes (String fileHash, String[] chunkHashesArray) {
        this.mFileHash = fileHash;
        this.mChunkHashesArray = chunkHashesArray;
    }
}
