package beeTorrent.utils;

import beeTorrent.files.FileAndChunkHashes;
import beeTorrent.roles.Bee;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class ioUtils {

    /**
     * Keyboard helper
     */
    public static Scanner mKbScanner;

    /**
     * Easy printing
     * @param s String to print
     */
    public static void ep(String s) {
        System.out.println(s);
    }

    /**
     * Easy printing int value
     * @param i int value to print
     */
    public static void ep(int i) {
        System.out.println(String.valueOf(i));
    }

    /**
     * Easy Logging
     * @param bee to log
     * @param content to log
     */
    public static void el(Bee bee, String content) {
        bee.mLog.add(bee.promptIdentity + content);
    }

    /**
     * Easy Logging
     * @param bee
     * @param content
     * @param alsoPrint
     */
    public static void el(Bee bee, String content, Boolean alsoPrint) {
        String print = bee.promptIdentity + content;

        bee.mLog.add(print);

        if(alsoPrint){
            ep(print);
        }
    }

    /**
     * Log read method
     * @param bee source bee
     * @return String to print
     */
    public static String logRead(Bee bee) {
        return String.join("\n", bee.mLog);
    }

    /**
     * Jar finding
     * @return Get info File
     */
    static String getJarName(){
        return new java.io.File(ioUtils.class.getProtectionDomain()
            .getCodeSource()
            .getLocation()
            .getPath())
            .getName();
    }

    /**
     * File utils. Generate Work Folder.
     * @param bee
     * @return work folder
     */
    public static File generateWorkFolder(Bee bee) {
        File workFolder;

        try {
            File jarPath = new File(ioUtils.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
            if (configUtils.workFolder == null) {
                workFolder = new File(jarPath.getParentFile().getAbsolutePath());
            } else {
                workFolder = new File(configUtils.workFolder);
            }
            workFolder.mkdirs();
            return workFolder;
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return workFolder = new File(System.getProperty("user.home"));
        }
    }

    /**
     * List of generate File List
     * @param folder working folder
     * @return List of files on the folder
     */
    public static List<File> generateFileList(File folder) {
        return Arrays.asList(folder.listFiles());
    }


    /**
     * Generate Files and chunk Hashes
     * @param file to generate the hashes
     * @return FileChunkHashes of the file
     */
    public static FileAndChunkHashes generateFileAndChunkHashes(File file) {
        try {
            MessageDigest fileMD = MessageDigest.getInstance("MD5");
            MessageDigest chunkMD = MessageDigest.getInstance("MD5");

            String fileHash;
            List<String> chunkHashList = new ArrayList<>();

            byte[] buffer = new byte[1024*512]; // 512KiB
            int read;

            InputStream is = new FileInputStream(file);

            while( (read=is.read(buffer)) != -1){
                fileMD.update(buffer,0,read);
                chunkHashList.add(DatatypeConverter.printHexBinary(chunkMD.digest(buffer)));
            }

            fileHash = DatatypeConverter.printHexBinary(fileMD.digest());

            return new FileAndChunkHashes(fileHash, chunkHashList.toArray(new String[0]));

        } catch (NoSuchAlgorithmException|IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] getByteArrayFromFile(File file, int offset) {

        try {
            byte[] buffer = new byte[1024*512]; // 512KiB

            FileInputStream is = new FileInputStream(file);

            // Set the pointer at the desired offset
            is.getChannel().position(configUtils.CHUNK_SIZE * 1024 * offset);

            is.read(buffer);

            return buffer;


        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }



}
