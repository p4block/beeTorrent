package beeTorrent.files;

import beeTorrent.utils.ioUtils;

import java.io.File;
import java.io.Serializable;

public class SharedFile implements Serializable  {

    /**
     * Name of shared file
     */
    public String name;

    /**
     * Path of shared file
     */
    public String path;

    /**
     * File Hash
     */
    public String fileHash;

    /**
     * List with all chunk hash
     */
    public String[] chunkHashList;

    /**
     * Constructor of shared File
     * @param file File to share with the swarm
     */
    public SharedFile (File file) {
        FileAndChunkHashes thing = ioUtils.generateFileAndChunkHashes(file);
        name = file.getName();
        path = file.getAbsolutePath();
        fileHash = thing.mFileHash;
        chunkHashList = thing.mChunkHashesArray;
    }

    /**
     * toString method for shared file
     * @return Name of the file shared
     */
    @Override
    public String toString() {
        return name;
    }
}
