package beeTorrent.communication;


import java.io.*;
import java.net.Socket;

import beeTorrent.packets.MessagePacket;
import beeTorrent.roles.Bee;
import beeTorrent.roles.Bee.HiveRole;

import static beeTorrent.utils.ioUtils.*;

public class ConnectionHandlerRunnable implements Runnable {

    /**
     * Connection Socket
     */
    private Socket mSocket;

    /**
     * Bee
     */
    private static Bee mCaller;

    /**
     * Role of Bee
     */
    private static HiveRole mRole;

    /**
     * Constructor ConnectionHandlerRunneable
     * @param caller Bee Caller
     * @param role Role of caller
     * @param socket Socket
     */
    public ConnectionHandlerRunnable(Bee caller, HiveRole role, Socket socket) {
        mCaller = caller;
        mRole = role;
        mSocket = socket;
    }

    /**
     * Thread method
     */
    public void run() {
        try {
            //el(mCaller, "Thread handling connection on port " + String.valueOf(mSocket.getPort()));

            try (ObjectInputStream mObjectInputStream = new ObjectInputStream(mSocket.getInputStream())) {
                MessagePacket mReceivedPacket = (MessagePacket) mObjectInputStream.readObject();

                el(mCaller, "Received from " + mSocket.getInetAddress() + ":" + mSocket.getPort() +
                        " with type: " + mReceivedPacket.Type);

                // Call Daddy to solve all your problems
                MessagePacket mResponsePacket = mCaller.handleMessage(mSocket, mReceivedPacket);

                ObjectOutputStream mObjectOutputStream = new ObjectOutputStream(mSocket.getOutputStream());
                mObjectOutputStream.writeObject(mResponsePacket);
                mObjectOutputStream.close();

            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            } finally {

                //el(mCaller, "Thread connection ended on port " + mSocket.getPort());
                mCaller.mSocketList.remove(mSocket);
                mSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
