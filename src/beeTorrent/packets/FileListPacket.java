package beeTorrent.packets;

import beeTorrent.files.SharedFile;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class FileListPacket extends MessagePacket implements Serializable  {


    /**
     * List of Shared Files
     */
    public List<SharedFile> mSharedFilesList;

    /**
     * Constructor Method
     * @param uuid UUID
     * @param sharedFilesList List of shared files
     */
    public FileListPacket(UUID uuid, List<SharedFile> sharedFilesList) {
        super.Type = MessageType.FILELIST;
        super.mUUID = uuid;
        mSharedFilesList = sharedFilesList;
    }

    /**
     * Print Type
     */
    public void Print() {
        System.out.println("Type:   " + Type);
    }

}