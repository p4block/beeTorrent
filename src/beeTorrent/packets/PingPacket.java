package beeTorrent.packets;

import java.io.Serializable;
import java.util.UUID;

public class PingPacket extends MessagePacket implements Serializable  {

    /**
     * String Content for Ping
     */
    public String Content;

    /**
     * Constructor Method. Setting Ping Packet
     * @param uuid UUID set
     * @param content Content set
     */
    public PingPacket(UUID uuid, String content) {
        super.Type = MessageType.PING;
        super.mUUID = uuid;
        this.Content = content;
    }

    /**
     * Print type
     */
    public void Print() {
        System.out.println("Type:   " + Type);
    }

 }