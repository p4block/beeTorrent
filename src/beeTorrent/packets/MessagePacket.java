package beeTorrent.packets;

import java.io.Serializable;
import java.util.UUID;

import static beeTorrent.packets.MessagePacket.MessageType.*;

public class MessagePacket implements Serializable {

    /**
     * Messages Types
     */
    public enum MessageType {
        DANK,
        DANKNT,
        PING,
        REGISTER,
        FILELIST,
        PEERLIST,
        CHUNK,
        GET
    }

    /**
     * Message Type set
     */
    public MessageType Type;

    /**
     * UUID set
     */
    public UUID mUUID;

    /**
     * Constructor Method. Type DANK
     */
    public MessagePacket() {
        Type = DANK;
    }

    /**
     * Constructor Method.
     * @param uuid UUID to set
     * @param type Type to set
     */
    public MessagePacket(UUID uuid, MessageType type) {
        Type = type;
        mUUID = uuid;
    }

    /**
     * Print type
     */
    public void Print() {
        System.out.println("Type:   " + Type);
    }



}