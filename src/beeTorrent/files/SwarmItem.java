package beeTorrent.files;

import beeTorrent.files.SharedFile;
import beeTorrent.roles.Bee;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.UUID;

public class SwarmItem  implements Serializable {

    /**
     * UUID Swarm
     */
    public UUID mUUID;

    /**
     * Role of Bee
     */
    private Bee.HiveRole Role;

    /**
     * IP of Bee
     */
    public InetAddress IP;

    /**
     * Port of Bee
     */
    public int Port;

    /**
     * List of files shared by peer
     */
    public List<SharedFile> mSharedFilesList;

    /**
     * Constructor Method. Add Item to swarm
     * @param uuid UUID
     * @param role Role of Bee
     * @param ip IP of Bee
     * @param port Port of Bee
     */
    public SwarmItem(UUID uuid, Bee.HiveRole role, InetAddress ip, int port) {
        mUUID = uuid;
        Role = role;
        IP = ip;
        Port = port;
    }

    /**
     * toString method.
     * @return Return String with role, ip and port of actual Bee
     */
    @Override
    public String toString() {
        return Role + "@" + IP + ":" + Port;
    }


}
