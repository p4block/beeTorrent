package beeTorrent.packets;

import beeTorrent.roles.Bee.HiveRole;

import java.io.Serializable;
import java.util.UUID;

public class GetPacket extends MessagePacket implements Serializable  {

    /**
     * Targets of Packet
     */
    // Probably adding more later
    public enum getTarget {
        PEERS
    }

    /**
     * Get Target
     */
    public getTarget mGetTarget;

    /**
     * Constructor Method.
     * @param uuid UUID
     * @param target Target
     */
    public GetPacket(UUID uuid, getTarget target) {
        super.Type = MessageType.GET;
        super.mUUID = uuid;
        mGetTarget = target;
    }

    /**
     * Print type
     */
    public void Print() {
        System.out.println("Type:   " + Type);
    }

}