package beeTorrent.roles;

import beeTorrent.files.SwarmItem;
import beeTorrent.packets.*;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import static beeTorrent.utils.ioUtils.el;

public class Tracker extends Bee {

    /**
     * Refer to this tracker
     */
    private Tracker mThis;

    /**
     * List of peers
     */
    public List<SwarmItem> mPeerList = new ArrayList<>();

    /**
     * Constructor Tracker. Will asime the role of tracker
     * @param port Port to link
     */
    public Tracker(int port){
        super(Bee.HiveRole.TRACKER, port);
        mThis = this;
    }

    /**
     * Handle Message.
     * @param socket Port for receiving incoming message
     * @param mMessagePacket Message
     * @return MessagePacket Message
     */
    @Override
    public MessagePacket handleMessage(Socket socket, MessagePacket mMessagePacket) {

        // Decode message
        MessagePacket.MessageType mType = mMessagePacket.Type;

        switch (mType){
            case PING:
                // Instantly respond with another ping
                PingPacket mPingPacket = (PingPacket) mMessagePacket;
                return new PingPacket(mUUID, mPingPacket.Content);
            case REGISTER:
                // Add peer to the list of peers
                RegisterPacket mRegisterPacket = (RegisterPacket) mMessagePacket;
                mPeerList.add(new SwarmItem(mMessagePacket.mUUID, HiveRole.PEER, socket.getInetAddress(), mRegisterPacket.Port));
                el(mThis, "Added peer " + socket.getInetAddress() + ":" + mRegisterPacket.Port);
                return new RegisterPacket(mUUID, mRole, mPort);
            case FILELIST:
                // Add packet's filelist to the swarmitem
                FileListPacket mFileListPacket = (FileListPacket) mMessagePacket;
                for(SwarmItem swarmItem : mPeerList) {
                    if(swarmItem.mUUID.equals(mMessagePacket.mUUID)) {
                        swarmItem.mSharedFilesList = new ArrayList<>(mFileListPacket.mSharedFilesList);
                    }
                }
                return new MessagePacket();
            case GET:
                GetPacket mGetPacket = (GetPacket) mMessagePacket;
                el(mThis, "Received GET from " + socket.getInetAddress() + ": " + mGetPacket.mGetTarget);
                if(mGetPacket.mGetTarget == GetPacket.getTarget.PEERS) {
                    return new PeerListPacket(mUUID, mPeerList);
                }
            default:
                return new MessagePacket(mUUID, MessagePacket.MessageType.DANKNT);
        }
    }

}
