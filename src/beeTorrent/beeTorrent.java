package beeTorrent;

import beeTorrent.roles.Peer;
import beeTorrent.utils.*;

import static beeTorrent.utils.configUtils.*;
import static beeTorrent.utils.docUtils.*;
import static beeTorrent.utils.ioUtils.*;
import static beeTorrent.utils.lifeCycleUtils.*;

import org.apache.commons.cli.*;
import org.apache.commons.cli.ParseException;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * Main class beeTorrent
 */
public class beeTorrent {

    //  _              _____                         _     //
    // | |__   ___  __|_   _|__  _ __ _ __ ___ _ __ | |_   //
    // | '_ \ / _ \/ _ \| |/ _ \| '__| '__/ _ \ '_ \| __|  //
    // | |_) |  __/  __/| | (_) | |  | | |  __/ | | | |_   //
    // |_.__/ \___|\___||_|\___/|_|  |_|  \___|_| |_|\__|  //

    /**
     * main class for execute the program
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {

        Options options;
        CommandLine cli;
        HelpFormatter formatter;

        CommandLineParser parser = new DefaultParser();
        formatter = new HelpFormatter();
        cli = null;

        // Add options to the argument parser
        Option helpOption = Option.builder("h")
                .longOpt("help")
                .required(false)
                .desc("Show this message")
                .build();

        Option roleOption = Option.builder("r")
                .longOpt("role")
                .numberOfArgs(1)
                .optionalArg(true)
                .required(false)
                .desc("Adopt specific role: tracker,peer,shell")
                .build();

        Option portOption = Option.builder("p")
                .longOpt("port")
                .numberOfArgs(1)
                .optionalArg(true)
                .required(false)
                .desc("Try to bind role to specific port")
                .build();

        Option debugOption = Option.builder("d")
                .longOpt("debug")
                .required(false)
                .desc("Enable stacktraces")
                .build();

        Option testOption   = Option.builder("t")
                .longOpt("test")
                .required(false)
                .desc("Automatically test a small network")
                .build();

        options = new Options();
        options.addOption(helpOption);
        options.addOption(roleOption);
        options.addOption(portOption);
        options.addOption(debugOption);
        options.addOption(testOption);

        // Parse said options
        try {
            cli = parser.parse(options, args);

        } catch (ParseException e) {
            formatter.printHelp(mCmdLineSyntax, options);
            System.exit(0);

        } finally {
            // Args aren't null, we can proceed to initialize the software
            // Allow the program to scan the keyboard
            ioUtils.mKbScanner = new Scanner(System.in);
        }

        // Decision variables
        int desiredPort = 0;
        boolean helpNeeded = false;
        // Avoids special case when returning from shell unnecessarily causes an exception
        boolean shellRun = false;

        //
        // Start of options parsing
        //

        if (cli.hasOption("help") || cli.getOptions().length == 0) {
            helpNeeded = true;
        }

        if (cli.hasOption("debug")){
            debugMode = true;
        }

        // Try to start specified role
        if (cli.hasOption("role")) {
            try {
                helpNeeded = false;
                switch (cli.getOptionValue("role")) {
                    case "tracker" :
                        if (cli.hasOption("port")) {
                            try {
                                desiredPort = Integer.parseInt(cli.getOptionValue("port"));

                            } catch (Exception e) {
                                helpNeeded = true;

                            } finally {
                                if (desiredPort < configUtils.MIN_PORT || desiredPort > configUtils.MAX_PORT) {
                                    ep("Invalid port. Valid range: "
                                            + configUtils.MIN_PORT + " " + configUtils.MAX_PORT);
                                }
                            }
                        } else {
                            desiredPort = configUtils.DEFAULT_PORT;
                        }
                        startTracker(desiredPort);
                        break;
                    case "peer" :
                        startPeer(desiredPort);
                        break;
                    case "shell" :
                        shellRun = true;
                        startShell();
                        break;
                    default :
                        helpNeeded = true;
                }
            } catch (Exception e) {
                if (debugMode) {
                    e.printStackTrace();
                }
                helpNeeded = true;
            }
        }

        if (cli.hasOption("test")) {
            ep("TESTMODE: STARTING TRACKER");
            startTracker(configUtils.DEFAULT_PORT);
            Thread.sleep(200);

            ep("\nTESTMODE: STARTING 3 PEERS");
            startPeer(7000);
            startPeer(7001);
            //startPeer(7002);


            Thread.sleep(300);

            ep("\nTESTMODE: READING PEER LOG");
            ep(logRead(mPeerList.get(0)));
            mPeerList.get(0).sendFilesList();

            Thread.sleep(500);

            mPeerList.get(1).getSwarmList();

            Thread.sleep(500);

            ep("\nTESTMODE: PEER 7001 LIST");
            mPeerList.get(1).printInfo();


        }

        // Print helpest
        if (helpNeeded && !shellRun) {
            formatter.printHelp(mCmdLineSyntax, options);
        }

    }

}
