package beeTorrent.roles;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import beeTorrent.communication.*;
import beeTorrent.packets.MessagePacket;
import beeTorrent.utils.lifeCycleUtils;

import static beeTorrent.utils.configUtils.debugMode;

import static beeTorrent.utils.ioUtils.el;


public class Bee {

    /**
     * Role Types. TRACKER or PEER
     */
    public enum HiveRole {
        TRACKER,
        PEER,
    }

    /**
     * Role
     */
    public HiveRole mRole;

    /**
     * Thread manager
     */
    private ExecutorService mExecutorService;

    /**
     * Event log
     */
    public List<String> mLog = new ArrayList<>();

    /**
     * Statelessless
     */
    private boolean threadRunning = false;

    /**
     * Socket object
     */
    private ServerSocket mServerSocket;

    /**
     * List of socket object
     */
    public List<Socket> mSocketList = new ArrayList<>();

    /**
     * Bee ID
     */
    public UUID mUUID;

    // Configuration
    //public InetAddress mIP;

    /**
     * Bee linked port on machine
     */
    public int mPort;

    /**
     * Referr to this object
     */
    Bee mThis;

    /**
     * Log property
     */
    public String promptIdentity;

    /**
     * Bee Constructor
     * @param role Tracker or Peer
     * @param port Port linked on machine
     */
    Bee (HiveRole role, int port) {
        mRole = role;
        mPort = port;
        mUUID = UUID.randomUUID();
        start();
    }

    /**
     * Early steps for init Bee
     */
    private void start(){
        mThis = this;
        mLog.add(mRole + " LOGFILE START");

        if (!threadRunning) {
            // Initialize thread manager
            mExecutorService = Executors.newCachedThreadPool();

            // Start server socket listener
            mExecutorService.execute(threadManagerRunnable);
        } else {
            el(mThis, "Already running", true);
        }
    }

    /**
     * Method for create threads with sockets. Thread Manager Bee
     */
    private Runnable threadManagerRunnable = () -> {
        try {
            mServerSocket = new ServerSocket(mPort);

            // If port wasn't selected, mPort is 0. Get true port.
            mPort = mServerSocket.getLocalPort();
            promptIdentity = mRole + "-" + mPort + ": ";

            el(mThis, "Started", true);

            // Setup work folder
            if (mThis instanceof Peer) {
                ((Peer) mThis).prepareWorkDir();
                // No auto registration for now
                //((Peer) mThis).registerOnTracker(InetAddress.getLocalHost());
            }

            threadRunning = true;

            while (threadRunning) {
                try {
                    Socket socket = mServerSocket.accept();
                    mSocketList.add(socket);
                    mExecutorService.execute(new ConnectionHandlerRunnable(mThis,mRole,socket));
                } catch (IOException e) {
                    el(mThis,"Stopping", true);
                }
            }

        } catch (IOException e) {
            if (debugMode) { e.printStackTrace(); }
            el(mThis, "Port " + mPort + " already in use. Unable to start.", true);
            // This object is of no use anymore. Clean ourselves up.
            getForgotten();
        }
    };

    /**
     * Method to sending a message
     * @param IP IP destination
     * @param port Port destination
     * @param messagePacket Message
     */
    void sendMessage(InetAddress IP, int port, MessagePacket messagePacket){
        MessageSenderRunnable mSenderThread = new MessageSenderRunnable(mThis, IP, port, messagePacket);
        mExecutorService.execute(mSenderThread);
    }

    /**
     * Method run when receiving a new message
     * @param socket Port for receiving incoming message
     * @param messagePacket Message
     * @return null?
     */
    public MessagePacket handleMessage(Socket socket, MessagePacket messagePacket) {
        return null;
    }

    /**
     * Method run when message sent has been responded
     * @param socket Port for receiving incoming message
     * @param messagePacket Message
     */
    public void handleResponse(Socket socket, MessagePacket messagePacket) {
    }

    /**
     * Method for try catch and stop running threads.
     */
    public void stop(){
        if (threadRunning) try {
            threadRunning = false;
            mServerSocket.close();
            mExecutorService.shutdownNow();
        } catch (IOException e) {
            // Socket closed successfully, false exception. Do nothing.
        }
    }

    /**
     * StopBee. Method that can be run from inside the runnable.
     */
    // Method that can be run from inside the runnable
    private void getForgotten(){
        lifeCycleUtils.stopBee(mThis);
    }

    /**
     * Method that return string with Bee Port
     * @return Port of Bee
     */
    public String toString(){
        return String.valueOf(mPort);
    }

}
