package beeTorrent.packets;

import beeTorrent.roles.Bee.HiveRole;

import java.io.Serializable;
import java.util.UUID;

public class RegisterPacket extends MessagePacket implements Serializable  {

    /**
     * Role of Bee
     */
    public HiveRole Role;

    /**
     * Port of Bee
     */
    public int Port;

    /**
     * Constructor Method. Add Packet to swarm
     * @param uuid UUID set
     * @param role Role of Bee
     * @param port Assign Port
     */
    public RegisterPacket(UUID uuid, HiveRole role, int port) {
        super.Type = MessageType.REGISTER;
        super.mUUID = uuid;
        this.Role = role;
        this.Port = port;
    }

    /**
     * Print Type
     */
    public void Print() {
        System.out.println("Type:   " + Type);
    }

}